from cone.commands.deploy import deploy
from cone.commands.revert import revert
from cone.commands.test import test
from cone.commands.version import version
